from docx import Document
from docx.shared import Pt,Length
from htmldocx import HtmlToDocx
import io
from flask import Flask, request, send_file, stream_with_context,  Response

import docx
from docx.shared import Cm

app = Flask(__name__)


def __html_to_docx(html, f_name):
    document = Document()
    new_parser = HtmlToDocx()

    #todo: https://python-docx.readthedocs.io/en/latest/user/text.html#line-spacing
    new_parser.add_html_to_document(html, document)
    sections = document.sections
    for section in sections:
      section.top_margin = Cm(4)
      section.bottom_margin = Cm(2)
      section.left_margin = Cm(4.5)
      section.right_margin = Cm(2)
    #todo: quedo hardcodeado el formato que necesita SanBenito
    #esto deberia pasarse por parametro post tambien
    for parag in document.paragraphs:
        parag.paragraph_format.line_spacing = 1.5
        for run in parag.runs:
            font    = run.font
            font.name = 'Times New Roman'
            font.size = Pt(12)

    file_stream = io.BytesIO()

    document.save(file_stream)

    response = Response(file_stream.getvalue())

    response.headers['Content-Type'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    response.headers['Content-Disposition'] = 'attachment; filename={}'.format(f_name.encode().decode('latin-1'))

    return response

@app.route("/", methods=['GET'])
def test():
    return "OK"

@app.route("/htmltodocx", methods=['POST'])
def gtml_to_docx():
    if request.method == 'POST' and request.form.get('html',False):
        return __html_to_docx(request.form['html'], 'report_'+request.form.get('file_name','noname')+'.docx')

@app.route("/test", methods=['GET'])
def test_html():
    if request.args.get('html'):
        return __html_to_docx(request.args['html'], 'report_TEST.docx')





if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
